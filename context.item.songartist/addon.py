# Show Song Artist Context Menu (https://git.hcoop.net/?p=clinton/unknownlamer-kodi-addons.git)

# Copyright (c) 2015 Clinton Ebadi <clinton@unknownlamer.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import xbmc
import json

def main ():
    artist = sys.listitem.getMusicInfoTag().getArtist()
    artistid = json.loads (xbmc.executeJSONRPC (json.dumps ({ 'jsonrpc': '2.0',
                                                              'id': '1',
                                                              'method':  'AudioLibrary.GetArtists',
                                                              'params': { 'filter': { 'field': 'artist',
                                                                                      'operator': 'is',
                                                                                      'value' : artist }}})))['result']['artists'][0]['artistid']
    xbmc.executebuiltin ('ActivateWindow (Music, "musicdb://artists/%s/")' % artistid)

if __name__ == '__main__':
    main ()
