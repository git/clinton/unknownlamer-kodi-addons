#!/usr/bin/python3
# Simple PartyMode Web Console
# Copyright (c) 2015,2016  Clinton Ebadi <clinton@unknownlamer.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Trivial xbmc admin script to view active playlist, control volume,
# etc.

# I would not recommend putting this online, no attempt is made at
# being even trivially secure (e.g. form values are passed directly to
# kodi with zero verification)

# ADD COMMAND TO RESTART PARTY MODE
# (probably should require confirmation)
# also add undelete link to post-del link just in case (reinsert at old pos)

# todo
# any kind of error checking

import cgi, cgitb
from datetime import datetime
import hashlib
import html
import numbers
import os
import random
import subprocess
import urllib
from kodijson import Kodi
from yattag import Doc

import partyparty
from partyparty import Song, SongControls, Search, Playlist, PartyManager

cgitb.enable()
#PAGE_SELF = os.environ['SCRIPT_NAME'] if 'SCRIPT_NAME' in os.environ else ''
PAGE_SELF = os.environ['SCRIPT_NAME'].rsplit('/', 1)[-1] if 'SCRIPT_NAME' in os.environ else ''
#PAGE_SELF = 'admin.cgi'

print ("content-type: text/html; charset=utf-8\n\n")
print ("<!DOCTYPE html>\n<html><head><title>partyparty beb</title></head><body>")
print (partyparty.css ())

#print (os.environ)
#print (os.environ['SCRIPT_NAME'])

def show_menu ():
   doc, tag, text = Doc().tagtext()
   with tag ('ul', klass = 'horiz-menu flex_row'):
      for target, description in [('#playlist', 'playlist'),
                                  ('#controls', 'controls'),
                                  ('javascript:document.getElementById("quicksearch").focus()', 'search'),
                                  (PAGE_SELF, 'reload')]:
         with tag ('li'):
            with tag ('a', href = target):
               text (description)

   print (doc.getvalue ())

xbmc = partyparty.connect (Kodi ("http://localhost:8080/jsonrpc"))

def print_escaped (item):
   print (u"<p>{}</p>".format (html.escape (u"{}".format (item))))

show_menu ()

manager = PartyManager (cgi.FieldStorage ())
manager.process ()

playlist = Playlist()
#playpos = random.randint (1, totalitems / (1 if 'asap' not in form else 3))

class PlayerControls:
   def __init__ (self, name='controls'):
      self.name = name

   def info (self):
      doc, tag, text = Doc().tagtext()
      _playtime = xbmc.Player.GetProperties (playerid=0, properties = ['position', 'percentage', 'time', 'totaltime'])
      pt = _playtime['result'] if 'result' in _playtime else None
      with tag ('ul', klass = 'horiz-menu'):
         for infotext in ['Volume {}%'.format(xbmc.Application.GetProperties (properties=['volume'])['result']['volume']),
                          'Time {:02d}:{:02d} / {:02d}:{:02d} ({:.2f}%) @ {:%H:%M:%S}'.format (pt['time']['hours'] * 60 +  pt['time']['minutes'], pt['time']['seconds'], pt['totaltime']['hours'] * 60 + pt['totaltime']['minutes'], pt['totaltime']['seconds'], pt['percentage'], datetime.now())]:
            with tag ('li'):
               text (infotext)

      return doc.getvalue ()



controls = PlayerControls ()
print (controls.info ())

print ('<a name="controls"></a>')
print ('''
<form method="post" action="{}" style="display: inline-block">
<button name="volchange" value="5" type="submit">+5</button>
<button name="volchange" value="-5" type="submit">-5</button>

<button name="volchange" value="10" type="submit">+10</button>
<button name="volchange" value="-10" type="submit">-10</button>

<button name="volmute" value="1">Toggle Mute</button>

</form>
'''.format (html.escape (PAGE_SELF)))

print ('''
<form method="post" action="{}" style="display: inline-block">
<button name="navigate" value="prev" type="submit">&#x23ee;</button>
<button name="navigate" value="next" type="submit">&#x23ed;</button>
<button name="navigate" value="playpause" type="submit">&#x23ef;</button>
</form>
'''.format (html.escape (PAGE_SELF)))

print ('<a name="playlist"></a><h1>Playlist</h1>')
print (playlist.show ())




print ('<a name="search"></a>')
Search ().show_quick_search (thereal=True)
show_menu ()

print ('<form method="post" action="{}" style="display: inline-block">'.format (html.escape (PAGE_SELF)))
print ('<button name="partyon" value="true">re-enable party</button>')
#print ('<button name="lockon" value="true">lock em out</button>')
print ('<button name="lights" value="on">lights on</button>')
print ('<button name="lights" value="off">lights off</button>')
print ('</form>')
print ('</body></html>')
