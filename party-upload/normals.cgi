#!/usr/bin/python3

# Simple PartyMode Web Console
# Limited interface for normal party goers
# Copyright (c) 2015,2016  Clinton Ebadi <clinton@unknownlamer.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# limit playlist view to next five

import cgi, cgitb
from datetime import datetime
import hashlib
import numbers
import os
import random
import subprocess
import sys
from kodijson import Kodi
from yattag import Doc

import partyparty
from partyparty import Song, SongControls, Search, Playlist, PartyManager

cgitb.enable()

print ("content-type: text/html; charset=utf-8\n\n")
sys.stdout.flush ()
print ("<!DOCTYPE html>\n<html><head><title>partyparty beb</title></head><body>")
print (partyparty.css ())
# Override max-width of song since controls will be zero width
print ('<style>.flex_row p { font-size: 175%; max-width: 100% !important; min-width: 50% }</style>');

PAGE_SELF = os.environ['SCRIPT_NAME'].rsplit('/', 1)[-1] if 'SCRIPT_NAME' in os.environ else ''

print ('<p style="font-size: 5rem"><a href="upload.html">upload a song</a> | <a href="youtube.html">add from YouTube</a> | <a href="normals.cgi">reload</a> | <a href="normals.cgi?browseartists=1">browse library</a>')




class NormalsPlaylist (Playlist):
    def get_playlist (self):
        return Playlist.get_playlist (self)#[0:10]

class NormalsManager (PartyManager):
    DEFAULT_QUEUE_DIVISOR = 1    

xbmc = partyparty.connect (Kodi ("http://localhost:8080/jsonrpc"))

manager = NormalsManager (cgi.FieldStorage ())
manager.process ()
playlist = NormalsPlaylist ()

print ('<a name="search"></a>')
Search ().show_quick_search (thereal=True)

print ('<a name="playlist"></a><h1>What\'s Playing</h1>')
print (playlist.show (controls = []))

print ('</body></html>')
