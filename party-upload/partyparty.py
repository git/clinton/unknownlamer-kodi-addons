# Kodi PartyParty Web Thing Library
# Part of Simple PartyMode Web Console
# Copyright (c) 2015 Clinton Ebadi <clinton@unknownlamer.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Copyright (c) 2015 Clinton Ebadi <clinton@unknownlamer.org>

import cgi, cgitb
from datetime import datetime
import hashlib
import html
import itertools
import numbers
import os
import random
import re
import subprocess
import urllib
import urllib.parse
from kodijson import Kodi
from yattag import Doc
import youtube_dl

kodi = None

def connect (_kodi):
    global kodi
    kodi = _kodi
    return kodi

SONG_PROPERTIES = ['album', 'artist', 'albumartist', 'title', 'dateadded', 'userrating', 'displayartist']
#PAGE_SELF = os.environ['SCRIPT_NAME'] if 'SCRIPT_NAME' in os.environ else ''
PAGE_SELF = os.environ['SCRIPT_NAME'].rsplit('/', 1)[-1] if 'SCRIPT_NAME' in os.environ else ''


class Song:
   def __init__ (self, song):
      self._song = song
      if 'artist' in song and len(song['artist']) > 0:
         self.artist = song['artist'][0]
      if 'displayartist' in song:
          self.artist = song['displayartist']
      elif 'albumartist' in song and len(song['albumartist']) > 0:
         self.artist = song['albumartist'][0]
      else:
         self.artist = 'who fucking knows'

      if 'album' in song and len(song['album']) > 0:
         self.album = song['album']
      else:
         self.album = 'album is for losers'

      if 'id' in song:
         # item from playlist
         self.key = hashlib.sha256(str(song['id']).encode('utf-8')).hexdigest()
         self.kodi_id  = song['id']
         # the playlist will not update things like ratings if we
         # update via RPC. Just grab it from the library instead.
         if 'userrating' in song:
            libsong = kodi.AudioLibrary.GetSongDetails (songid = song['id'], properties = ['userrating'])
            #print (libsong)
            if 'result' in libsong and 'songdetails' in libsong['result']:
               song['userrating'] = libsong['result']['songdetails']['userrating']
      elif 'songid' in song:
         # search results
         self.key = hashlib.sha256(str(song['songid']).encode('utf-8')).hexdigest()
         self.kodi_id  = song['songid']
      else:
         self.key = hashlib.sha256((song['label'] + self.artist).encode('utf-8')).hexdigest()
         self.kodi_id = 0

      # videos can still be labeled as songs, but the rating will be a
      # float...
      if 'userrating' in song and isinstance (song['userrating'], numbers.Integral):
         self.rating = song['userrating']
      else:
         self.rating = -1 # might be better to use None here

      if 'title' in song and len(song['title']) > 0:
          self.label = song['title']
      else:
          self.label = song['label']

def songs(items):
   '''Convert list of Kodi Items into Song instances'''
   return [Song(item) for item in items]

def get_playlist (playlistid=0):
   return songs (kodi.Playlist.GetItems (playlistid=playlistid, properties=SONG_PROPERTIES)['result']['items'])

class SongControls:
   aactions = {'songup': 'up', 'songdown': 'down', 'songtop': 'next!', 'songbottom': 'banish!',
               'songdel': 'del', 'randomqueue': 'yeh', 'songrate': 'rate'}

   def __init__ (self, song, actions = ['songup', 'songdown', 'songdel']):
      self.song = song
      self.actions = actions

   def controls (self):
      doc, tag, text = Doc().tagtext()
      with tag ('form', method = 'post', action = PAGE_SELF, klass = 'song_controls'): #, style = 'display: inline-block'):
         for action in self.actions:
            with tag ('button', name = action, value = self.song.key):
               text (self.aactions[action])
            doc.asis (self.extra_elements (action))
      return doc.getvalue()

   def extra_elements (self, action):
      doc, tag, text = Doc().tagtext()
      if action == 'randomqueue':
         doc.stag ('input', type = 'hidden', name = 'songkodiid', value = self.song.kodi_id)
      elif action == 'songrate':
         if self.song.rating > -1:
            doc.defaults = {'songrating': self.song.rating}
            with doc.select (name = 'songrating'):
               with doc.option (value = 0):
                  text ('na')
               for i in range (1,6):
                  with doc.option (value = i*2):
                     text (str (i))
            doc.stag ('input', type = 'hidden', name = 'songkodiid', value = self.song.kodi_id)

      return doc.getvalue ()

class Search:
   ANY_SEARCH_PROPERTIES = [ 'title', 'album', 'artist' ]

   def __init__ (self, term = '', prop = 'title'):
      self.term = term
      self.prop = prop
      if (term != ''):
         self.results = self.get_search_results ()

   def get_search_results (self):
       if (self.term == ''):
           return {}

       if (self.prop != 'any'):
           res = kodi.AudioLibrary.GetSongs (filter={'operator': "contains", 'field': self.prop, 'value': self.term}, properties=SONG_PROPERTIES, sort={'order': 'ascending', 'method': 'artist'})['result']
           if 'songs' in res:
               return songs(res['songs'])
           else:
               return []
       else:
           all_songs = [kodi.AudioLibrary.GetSongs (filter={'operator': "contains", 'field': p, 'value': self.term}, properties=SONG_PROPERTIES, sort={'order': 'ascending', 'method': 'artist'})['result']
                        for p
                        in self.ANY_SEARCH_PROPERTIES]
           # does not remove duplicates...
           return list (itertools.chain.from_iterable ([res['songs'] for res in all_songs if 'songs' in res]))

   def show_quick_search (self, thereal=False):
      doc, tag, text = Doc(defaults = {'searchfield': self.prop}).tagtext()
      with tag ('form', method = 'get', action = PAGE_SELF, style = 'display: inline-block'):
          if thereal:
              doc.stag ('input', type = 'text', name = 'searchterm', value = self.term, id = 'quicksearch')
          else:
              doc.stag ('input', type = 'text', name = 'searchterm', value = self.term)
          with doc.select (name = 'searchfield'):
              for prop in ['title', 'artist', 'album', 'any']:
                  with doc.option (value = prop):
                      text (prop)
          with tag ('button', type = 'submit', name = 'searchgo', value = '1'):
              text ('Search')
      print (doc.getvalue ())

   def show_search_results (self):
      doc, tag, text = Doc().tagtext()
      with tag ('h1'):
         text ('Results')
      if len (self.results) > 0:
          doc.asis (Playlist (self.results).show (['randomqueue']))
      else:
         with tag ('p'):
            text ('You are unworthy. No results.')

      print (doc.getvalue ())

class Playlist:
   default_controls = ['songup', 'songdown', 'songdel', 'songrate', 'songtop', 'songbottom']
   def __init__ (self, kodi_playlist = None):
      if kodi_playlist is None:
         self.playlist = self.get_playlist ()
      elif (all (isinstance (s, Song) for s in kodi_playlist)):
         self.playlist = kodi_playlist
      else:
          self.playlist = songs (kodi_playlist)

   def show (self, controls = default_controls):
      doc, tag, text = Doc().tagtext()
      with tag ('ol', klass = 'flex_list'):
         for song in self.playlist:
#            text ("{}".format (song._song))
            with tag ('li'):
               with tag ('div', klass = 'flex_row'):
                  with tag ('p'):
                     with tag ('a', href = '{1}?searchgo=1&searchterm={0}&searchfield=artist'.format(song.artist, PAGE_SELF)):
                        text (song.artist)
                     text (' ({}) {}'.format(song.album, song.label))
                  doc.asis (SongControls (song, controls).controls())
      return doc.getvalue ()

   def get_playlist (self, playlistid=0):
      return songs (kodi.Playlist.GetItems (playlistid=playlistid, properties=SONG_PROPERTIES)['result']['items'])

class Upload:
    upload_dir = '/srv/archive/incoming/stolen-moosic'

    def __init__ (self, form, field):
        self.fileitem = form[field]
        self.filename = '{}/{}'.format (self.upload_dir, self.fileitem.filename)

    # Evil: just run replaygain/mp3gain/metaflac on the file and hope one
    # works instead of dealing with MIME. For now.
    def attempt_rpgain (self):
        subprocess.call (["/usr/bin/vorbisgain", "-q", self.filename])
        subprocess.call (["/usr/local/bin/mp3gain", "-q", "-s", "i", self.filename])
        subprocess.call (["/usr/bin/aacgain", "-q", "-s", "i", self.filename])
        subprocess.call (["/usr/bin/metaflac", "--add-replay-gain", self.filename])

    def save (self):
        fout = open (os.path.join(self.upload_dir, self.fileitem.filename), 'wb')
        fout.write (self.fileitem.value)
        fout.close()
        self.attempt_rpgain ()
        return { 'file': self.filename }

class Youtube:
    upload_dir = '/srv/archive/incoming/youtube-moosic'
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': upload_dir + '/%(title)s-%(id)s.%(ext)s',
        'quiet': True,
        'postprocessors': [
            {
                'key': 'FFmpegMetadata',
            },
            {
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'vorbis',
            }],

    }

    def __init__ (self, form, field):
        self.ydl = youtube_dl.YoutubeDL(self.ydl_opts)
        self.url = form.getvalue (field)

    def save (self):
        info = self.ydl.extract_info (self.url, download=True)
        filename = re.sub ('\..{3,4}$', '.ogg', self.ydl.prepare_filename (info))
        subprocess.call (["/usr/bin/vorbisgain", "-q", filename])
        return { 'file': filename }


def css ():
    doc, tag, text = Doc ().tagtext ()
    with tag ('style'):
      text ('''
input, select, button { font-size: 200%; margin: 0.1em; }
.horiz-menu li { display: inline; padding-right: 0.5em; font-size: 1.75rem; }
body {  /* background-image: url("fire-under-construction-animation.gif");  */
  color: white;
  background-color: black;
  font-family: sans-serif;
}
a { color: #5dfc0a}
button[name=songdel] { margin-left: 1em; margin-right: 1em; }
.flex_row {
 display: flex;
 flex-flow: row nowrap;
 justify-content: space-between;
 align-items: center;
}

.flex_row p { font-size: 175%; max-width: 50%; min-width: 50% }

ol li:nth-child(even) { background-color: #202020 }
''')
    return doc.getvalue ()

def print_escaped (item):
   print (u"<p>{}</p>".format (html.escape (u"{}".format (item))))

# This is awful
class PartyManager:
    DEFAULT_QUEUE_DIVISOR = 3

    def __init__ (self, form):
        self.form = form

    def randomqueue (self, item, divisor=None):
        # randomly queue song somewhere in first (playlist.length /
        # divisor) songs
        if divisor is None:
            divisor = self.DEFAULT_QUEUE_DIVISOR
        totalitems = kodi.Playlist.GetItems (playlistid=0)['result']['limits']['total']
        playpos = random.randint (1, int(totalitems / divisor + 1))
        print_escaped (kodi.Playlist.Insert (playlistid=0, item=item, position=playpos))
        print ('<p style="font-size: x-large">Your song is number {0} in the queue ({1} songs in playlist).</p>'.format (playpos, totalitems+1))
        return (playpos, totalitems+1)

    def process (self):
        form = self.form
        if 'songdel' in form:
            songid = form['songdel'].value
            print (u"<p>{}</p>".format (songid))
            (pos,song) = next ((i,s) for i,s in enumerate(get_playlist ()) if s.key == songid)
            print (u'<p>Deleted {}</p>'.format(html.escape (song.label)))
            print_escaped (kodi.Playlist.Remove (playlistid=0, position=pos))
        elif 'songup' in form:
            songid = form['songup'].value
            print (u"<p>{}</p>".format (songid))
            (pos,song) = next ((i,s) for i,s in enumerate(get_playlist ()) if s.key == songid)
            print (u"<p>Promoted {}</p>".format(html.escape(song.label)))
            print_escaped (kodi.Playlist.Swap (playlistid=0, position1=pos, position2=pos-1))
        elif 'songdown' in form:
            songid = form['songdown'].value
            print (u"<p>{}</p>".format (songid))
            (pos,song) = next ((i,s) for i,s in enumerate(get_playlist ()) if s.key == songid)
            print (u"<p>Demoted {}</p>".format(html.escape(song.label)))
            print_escaped (kodi.Playlist.Swap (playlistid=0, position1=pos, position2=pos+1))
        elif 'songtop' in form:
            songid = form['songtop'].value
            print (u"<p>{}</p>".format (songid))
            (pos,song) = next ((i,s) for i,s in enumerate(get_playlist ()) if s.key == songid)
            print (u"<p>Bumped Up {}</p>".format(html.escape(song.label)))
            for i in range (pos, 1, -1):
                print_escaped (kodi.Playlist.Swap (playlistid=0, position1=i, position2=i-1))
        elif 'songbottom' in form:
            songid = form['songbottom'].value
            print (u"<p>{}</p>".format (songid))
            playlist = get_playlist ()
            (pos,song) = next ((i,s) for i,s in enumerate(playlist) if s.key == songid)
            print (u"<p>Banished {}</p>".format(html.escape(song.label)))
            for i in range (pos, len (playlist), 1):
                print_escaped (kodi.Playlist.Swap (playlistid=0, position1=i, position2=i+1))
        elif 'volchange' in form:
            curvolume = kodi.Application.GetProperties (properties=['volume'])['result']['volume']
            newvolume = max (0, min (int (form['volchange'].value) + curvolume, 100))
            print_escaped (kodi.Application.SetVolume (volume=newvolume))
        elif 'volmute' in form:
            print_escaped (kodi.Application.SetMute (mute="toggle"))
        elif 'navigate' in form:
            action = form['navigate'].value
            if action == 'prev':
                print_escaped (kodi.Player.GoTo (to="previous", playerid=0))
            elif action == 'next':
                print_escaped (kodi.Player.GoTo (to="next", playerid=0))
            elif action == 'playpause':
                print_escaped (kodi.Player.PlayPause (play="toggle",  playerid=0))
        elif 'searchgo' in form:
            term = form['searchterm'].value
            field = form['searchfield'].value
            search = Search (term, field)
            search.show_quick_search ()
            search.show_search_results ()
        elif 'randomqueue' in form:
            songid = int(form['songkodiid'].value)
            self.randomqueue ({"songid": songid})
        elif 'songrate' in form:
            songid = int(form['songkodiid'].value)
            newrating = int(form['songrating'].value)
            print (songid)
            print (newrating)
            print_escaped (kodi.AudioLibrary.SetSongDetails (songid = songid, userrating = newrating))
            print_escaped (u'Rating Changed')
        elif 'browseartists' in form:
            artists = kodi.AudioLibrary.GetArtists (sort={'order': 'ascending', 'method': 'artist'})['result']['artists']
            doc, tag, text = Doc().tagtext()
            with tag ('ol', klass='flex_list'):
                for artist in artists:
                    with tag ('li', style='padding: 1rem; font-size: x-large'):
                        with tag ('a', href='{}?searchgo=1&searchterm={}&searchfield=artist'.format (PAGE_SELF, urllib.parse.quote_plus (artist['artist']))):
                            text (artist['label'])
            print (doc.getvalue ())
        elif 'uploadgo' in form:
            upload = Upload (form, 'song')
            item = upload.save ()
            self.randomqueue (item, 1 if 'asap' not in form else 3)
        elif 'youtubego' in form:
            youtube = Youtube (form, 'youtubeurl')
            item = youtube.save ()
            self.randomqueue (item, 1 if 'asap' not in form else 3)
        elif 'partyon' in form:
            if 'error' in kodi.Player.SetPartymode (partymode=True, playerid=0):
                kodi.Player.Open (item={"partymode": "music"})
        elif 'lockon' in form:
            subprocess.call (['/usr/bin/xscreensaver-command', 'lock'])
        elif 'lights' in form:
            subprocess.call (['/usr/bin/br', '-N' if form['lights'].value == 'on' else '-F'])
