#!/usr/bin/python3
# Simple PartyMode Web Console
# Copyright (c) 2015,2016 Clinton Ebadi <clinton@unknownlamer.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# todo:
# fix weird characters (see erica's song)
# use shutil.copyfileobj to possibly speed up copying
# - check if it actually causes chunked reads from the browser
# - LOL WHAT uploads are like 10s don't bother
# support multiple files
# - daemonize replaygain? NOPE it runs in like no time thanks to moar power
# track uploads (overcomplicated version)
# - phone ip is hopefully good enough
# - track # of uses of asap/Next Please
# - track files we have queued (need at least next-please so we can queue those in order)
# track uploads (easy version)
# - dump timestamps into file (/tmp = cleared at reboot, no need to clear it manually probably)
# - sum up # of songs uploading in last N minutes
# - throttle asap position based on the number of songs being uploaded
# - e.g. no one in last PERIOD = go next, fuckton of people = asap just ain't working


# integrate better with native queue


## crazy idea: if the playlist can be watched with a script... (maybe
## a json rpc notification? but ... reliability, and upload.cgi runs
## infrequently enough that it cannot record the playlist and extract
## a meaningful image of the queue ...).

# diff playlist whenever item is inserted (or song changes, or whatever we can hook on).
# scan for common items (discarding the head of the old snapshot, since we could just be advancing)
# when the first different item is found... if it ain't the end of the list it's been queued
# keep going until we match up again and record the end of the queue position in a file
# what about when we delete in the list?
#

# upload.html
# - show playlist length, # of next/asap available, likely queue position
# - show upcoming songs (5? just next-please?)
# - - just use SSI + python virtual includes, seriously

import cgi, cgitb
import os, sys
import daemon
import subprocess
import random
from kodijson import Kodi

import partyparty
from partyparty import Upload, PartyManager

cgitb.enable()

print "content-type: text/html\n\n"
sys.stdout.flush ()

form = cgi.FieldStorage ()

manager = PartyManager (form)

print '<p><a href="upload.html">Upload another song</a></p>'
sys.stdout.flush ()

xbmc = partyparty.connect (Kodi ("http://localhost:8080/jsonrpc"))
manager.randomqueue ({"file": filename}, 1 if 'asap' not in form else 3)

# todo: use REMOTE_ADDR to limit how many asap requests a person can
# make in a time period
#totalitems = xbmc.Playlist.GetItems (playlistid=0)['result']['limits']['total']
#playpos = random.randint (1, totalitems / (1 if 'asap' not in form else 3))

#print xbmc.Playlist.Insert (playlistid=0, item={"file": filename }, position=playpos)

#print '<p style="font-size: x-large">Your song is number {0} in the queue ({1} songs in playlist).</p>'.format (playpos, totalitems+1)
